# How to use

```
use mkascii::AsciiBuilder;

// You can use AsciiBuilder::from_bytes if you alread have the data in memory
let mut image_processor = AsciiBuilder::from_path("/path/to/image/file")
.with_rows_cols(10, 20) // This is optional, and if left out mkascii will detect this from your current terminal size
.build();

println!("{}", image_processor.to_string());

```
