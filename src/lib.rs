use image::*;
use std::path::PathBuf;
const ASCII_CHARS_LEN: usize =  12;
const SLOT_SIZE: usize =  255000000 / ASCII_CHARS_LEN;

const ASCII_CHARS: [char; ASCII_CHARS_LEN] = ['@', '#', '$', '%', '?', '*', '+', ';', ':', ',', '.', ' '];
//const ASCII_CHARS: [char; ASCII_CHARS_LEN] = [' ', '.', ',', ':', ';', '+', '*', '?', '%', '$', '#', '@'];

pub struct AsciiBuilder {
    rows: Option<usize>,
    cols: Option<usize>,
    image: GrayImage,
}

pub struct ImageAsciiProcessor {
    rows: usize,
    cols: usize,
    image: GrayImage,
}

impl ImageAsciiProcessor {
    pub fn to_string(&mut self) -> String {
        assert_eq!(SLOT_SIZE * ASCII_CHARS_LEN, 255000000);
        //self.cols = ((((self.cols as f32 / self.image.width() as f32) as f32) * (self.image.width() as f32 / self.cols as f32))) as usize;
        //println!("{}", self.cols);
        //assert_ne!(proper_size as usize, self.cols);
        //let image: &mut dyn GenericImageView<Pixel=Luma<u8>> = &self.image.into_luma8();
        //let image_buffer: ImageBuffer<Luma<u8>, GrayImage> = self.image.view;
        let mut output = String::new();
        let width = self.image.width();
        let height = self.image.height();
        self.cols = self.cols - ((self.rows * width as usize) / height as usize);

        //let ratio: usize = ((height / width) * 1000000) as usize;
        //println!("cols {}, rows {}", self.cols, self.rows);
        //let char_width: f32 = ((width * 1000000) / (self.cols as u32)) / 1000000;
        //let char_height: f32 = ((height * 1000000) / (self.rows as u32)) / 1000000;
        let char_width = width / self.cols as u32;
        let char_height = height / self.rows as u32;
        let mut chopped = vec![vec![GrayImage::default(); self.cols.into()]; self.rows.into()];
        self.image = DynamicImage::ImageLuma8(self.image.clone()).resize_exact(self.cols as u32 * char_width, self.rows as u32 * char_height, imageops::FilterType::Triangle).to_luma8();
        
        for x in 0..self.cols {
            //if x == self.cols - 1 && !skip_x {
            //    char_width += width - (x as u32 * char_width);
            //    skip_x = true;
            //}
            for y in 0..self.rows {
                //if y == self.rows - 1 && !skip_y {
                //    char_height += height - (y as u32 * char_height);
                //    skip_y = true;
                //}
                //println!("h = {}, w = {}", char_height, char_width);
                chopped[y][x] = self.image.view(x as u32 * char_width, y as u32 * char_height, char_width as u32, char_height as u32).to_image();
            }
        }
        
        for horizontal_slice in chopped {
            for section in horizontal_slice {
                let mut luma_values: Vec<usize> = Vec::new();
                for pixel in section.pixels() {
                    //println!("blah");
                    luma_values.push(pixel.0[0].into());
                }
                //println!("{:#?}", luma_values);
                let average: usize = ((luma_values.iter().sum::<usize>() * 1000000) / (luma_values.len() * 1000000)) * 1000000;
                //println!("Average {}", average);
                let mut tripped = false;
                for index in 0..ASCII_CHARS_LEN {
                    let var1 = average >= SLOT_SIZE * index;
                    let var2 = average < (SLOT_SIZE * (index + 1)) + 1;
                    if var1 && var2 {
                        output.push(ASCII_CHARS[index]);
                        tripped = true;
                        //println!("tripped = true");
                        break;
                    }
                    if tripped {
                        println!("Should have breaked!");
                    }
                }
                assert_eq!(!tripped, false);
            }
            output.push('\n');
        }
        output
    }
}

impl AsciiBuilder {
    pub fn from_path(path: impl Into<PathBuf>) -> Self {
        Self {
            rows: None,
            cols: None,
            image: image::io::Reader::open(path.into()).unwrap().decode().unwrap().to_luma8(),
        }
    }
    pub fn from_bytes(bytes: impl Into<Vec<u8>>) -> Self {
        let buf = bytes.into();
        Self {
            rows: None,
            cols: None,
            image: image::load_from_memory(buf.as_slice()).unwrap().to_luma8(),
        }
    }
    pub fn with_rows_cols(mut self, rows: usize, cols: usize) -> Self {
        self.rows = Some(rows);
        self.cols = Some(cols);
        self
    }
    pub fn build(self) -> ImageAsciiProcessor {
        let rows: usize;
        let cols: usize;
        if self.rows.is_none() || self.cols.is_none() {
            let sizes = termsize::get().unwrap();
            rows = sizes.rows.into();
            cols = sizes.cols.into();
        } else {
            rows = self.rows.unwrap();
            cols = self.cols.unwrap();
        }
        ImageAsciiProcessor {
            rows: rows,
            cols: cols,
            image: self.image,
        }
    }
}
